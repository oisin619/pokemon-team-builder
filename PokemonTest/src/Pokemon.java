import java.util.ArrayList;
import java.util.Vector;


public class Pokemon 
{
	private final int DEX_NUM;
	private final String name;
	private Nature nature;
	private Type t1;
	private Type t2;
	private Stat hp = new Stat("hp", 0);
	private Stat atk = new Stat("atk",0);
	private Stat def = new Stat("def",0);;
	private Stat spAtk  = new Stat("spAtk",0);;
	private Stat spDef = new Stat("spDef",0);;
	private Stat spe = new Stat("spe",0);;
	private Base_Stats base;
	private Ability ability;
	private Item item;
	private String gender;
	private Move move1;
	private Move move2;
	private Move move3;
	private Move move4;
	private ArrayList<Ability> possibleAbilities = new ArrayList<Ability>(3);
	private Vector<Move> possibleMoves = new Vector<Move>(100);
	private Pokemon originalForme;
	
	public Pokemon(int dex, String name, Nature nature,Type t1, Type t2,Ability ability,Base_Stats base, Item item, String gender, Move move1,Move move2,Move move3,Move move4,Pokemon originalForme)
	{
		this.DEX_NUM = dex;
		this.name = name;
		this.nature = nature;
		this.t1 = t1;
		this.t2 = t2;
		this.base = base;
		this.item = item;
		this.gender = gender;
		this.move1 = move1;
		this.move2 = move2;
		this.move3 = move3;
		this.move4 = move4;
		this.ability = ability;
		this.originalForme = originalForme;
	}
	
	

	public Ability getAbility()
	{
		return ability;
	}



	public void setAbility(Ability ability)
	{
		this.ability = ability;
	}



	public Move getMove1()
	{
		return move1;
	}


	public void setMove(Move move1)
	{
		this.move1 = move1;
	}
	
	


	public Move getMove2() 
	{
		return move2;
	}



	public void setMove2(Move move2) 
	{
		this.move2 = move2;
	}



	public Move getMove3() 
	{
		return move3;
	}



	public void setMove3(Move move3) 
	{
		this.move3 = move3;
	}



	public Move getMove4() 
	{
		return move4;
	}



	public void setMove4(Move move4) 
	{
		this.move4 = move4;
	}



	public Nature getNature()
	{
		return nature;
	}

	public void setNature(Nature nature)
	{
		this.nature = nature;
	}

	public Type getT1()
	{
		return t1;
	}

	public void setT1(Type t1)
	{
		this.t1 = t1;
	}

	public Type getT2()
	{
		return t2;
	}

	public void setT2(Type t2)
	{
		this.t2 = t2;
	}

	public Stat getHp()
	{
		return hp;
	}

	public void setHp(Stat hp)
	{
		this.hp = hp;
	}

	public Stat getAtk()
	{
		return atk;
	}

	public void setAtk(Stat atk)
	{
		this.atk = atk;
	}

	public Stat getDef()
	{
		return def;
	}

	public void setDef(Stat def)
	{
		this.def = def;
	}

	public Stat getSpAtk()
	{
		return spAtk;
	}

	public void setSpAtk(Stat spAtk)
	{
		this.spAtk = spAtk;
	}

	public Stat getSpDef()
	{
		return spDef;
	}

	public void setSpDef(Stat spDef)
	{
		this.spDef = spDef;
	}

	public Stat getSpe()
	{
		return spe;
	}

	public void setSpe(Stat spe)
	{
		this.spe = spe;
	}

	public Base_Stats getBase()
	{
		return base;
	}

	public void setBase(Base_Stats base)
	{
		this.base = base;
	}

	public Item getItem()
	{
		return item;
	}

	public void setItem(Item item)
	{
		this.item = item;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public int getDEX_NUM()
	{
		return DEX_NUM;
	}

	public String getName()
	{
		return name;
	}
	
	
	
	public Pokemon getOriginalForme() 
	{
		return originalForme;
	}



	public void setOriginalForme(Pokemon originalForme) 
	{
		this.originalForme = originalForme;
	}



	public double calcMultiply(Move move)
	{
		double multiply;
		multiply = move.getType().calcDamage(this.getT1(), this.getT2());
		return multiply;
	}
	
	
	
	
}
