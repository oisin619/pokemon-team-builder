import java.util.ArrayList;


public class Test
{

	public static void main(String[] args)
	{ 
		Ability blaze = new Ability("Blaze", "Fire-Type moves increase by 50% when user has less than 1/3 health remaining");
		Ability torrent = new Ability("Torrent", "Water-Type moves increase by 50% when user has less than 1/3 health remaining");
		Ability overgrow = new Ability("Overgrow", "Grass-Type moves increase by 50% when user has less than 1/3 health remaining");
		
		ArrayList<Type> fireWeaknesses = new ArrayList<Type>(10);
		ArrayList<Type> fireResistances = new ArrayList<Type>(10);
		ArrayList<Type> fireImmunites = new ArrayList<Type>(10);

		ArrayList<Type> waterWeaknesses = new ArrayList<Type>(10);
		ArrayList<Type> waterResistances = new ArrayList<Type>(10);
		ArrayList<Type> waterImmunites = new ArrayList<Type>(10);

		ArrayList<Type> grassWeaknesses = new ArrayList<Type>(10);
		ArrayList<Type> grassResistances = new ArrayList<Type>(10);
		ArrayList<Type> grassImmunites = new ArrayList<Type>(10);

		ArrayList<Type> poisonWeaknesses = new ArrayList<Type>(10);
		ArrayList<Type> poisonResistances = new ArrayList<Type>(10);
		ArrayList<Type> poisonImmunites = new ArrayList<Type>(10);

		ArrayList<Type> fightingWeaknesses = new ArrayList<Type>(10);
		ArrayList<Type> fightingResistances = new ArrayList<Type>(10);
		ArrayList<Type> fightingImmunites = new ArrayList<Type>(10);
		
		ArrayList<Ability> yeah = new ArrayList<Ability>();


		Type fire = new Type("Fire", fireWeaknesses, fireResistances, fireImmunites);
		Type water = new Type("Water",waterWeaknesses,waterResistances,waterImmunites);
		Type grass = new Type("Grass",grassWeaknesses,grassResistances, grassImmunites);
		Type poison = new Type("Poison",poisonWeaknesses,poisonResistances, poisonImmunites);
		Type fighting = new Type("Fighting",fightingWeaknesses,fightingResistances, fightingImmunites);
		
		fireWeaknesses.add(grass);
		fireResistances.add(water);
		fireResistances.add(fire);
		
		waterWeaknesses.add(fire);
		waterResistances.add(grass);
		waterResistances.add(water);
		
		grassWeaknesses.add(water);
		grassResistances.add(fire);
		grassResistances.add(poison);
		grassResistances.add(grass);
		
		poisonWeaknesses.add(grass);
		poisonResistances.add(poison);
		
		fightingResistances.add(poison);
		
		Move firePunch = new Move("Fire Punch", fire, "Physical", 15, 75, 100);
		Move hydroPump = new Move("Hydro Pump", water, "Special", 5, 120, 85);
		Move leafBlade = new Move("Leaf Blade", grass, "Physical", 15, 90, 100);
		
		Item sitrusBerry = new Item("Sitrus Berry", "Restores a Pokemon's HP by 1/4, if held the user will consume it when HP drops below 1/2");
		
		Nature lonely = new Nature("Lonely", "atk", "def");
		Nature bold = new Nature("Bold", "def", "atk");
		Nature serious = new Nature("Serious", "none", "none");
		
		int ev = 0;
		int iv = 31;
		int level = 100;
		
		Pokemon blaziken = new Pokemon(257,"Blaziken",serious, fire, fighting,blaze,new Base_Stats(108,130,95,80,85,102), sitrusBerry,"Female",firePunch,null,null,null,null);
		
		Pokemon venusaur = new Pokemon(3,"Venusaur",lonely, grass, poison,overgrow,new Base_Stats(80,105,65,100,70,70), sitrusBerry,"Male",leafBlade,null,null,null,null);
		
		Pokemon blastoise = new Pokemon(9,"Blastoise",bold, water, null,torrent,new Base_Stats(78,84,78,109,85,100), sitrusBerry,"Male",hydroPump,null,null,null,null);
		

		
		
		venusaur.getHp().calcHP(venusaur.getBase(), ev, iv, level);
		venusaur.getAtk().calcStat(venusaur.getBase(), ev, iv, level, venusaur.getNature());
		blaziken.getAtk().calcStat(blaziken.getBase(), 252, iv, level, blaziken.getNature());
		blaziken.getDef().calcStat(blaziken.getBase(), ev, iv, level, lonely);
		blastoise.getDef().calcStat(blastoise.getBase(), 252, 31, level, blastoise.getNature());
		
		System.out.println(venusaur.getHp().getStat());
		System.out.println(venusaur.getAtk().getStat());
		System.out.println(blaziken.getAtk().getStat());
		System.out.println(blaziken.getDef().getStat());
		System.out.println(blastoise.getDef().getStat());
		
		
		
		

	}

}
