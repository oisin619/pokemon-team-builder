
public class Nature 
{
	private String name;
	private String posStat;
	private String negStat;
	
	public Nature(String name, String posStat, String negStat)
	{
		this.name = name;
		this.posStat = posStat;
		this.negStat = negStat;
	}
	
	
	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}


	public String getPosStat() 
	{
		return posStat;
	}

	public void setPosStat(String posStat) 
	{
		this.posStat = posStat;
	}

	public String getNegStat() 
	{
		return negStat;
	}

	public void setNegStat(String negStat) 
	{
		this.negStat = negStat;
	}
	
}
