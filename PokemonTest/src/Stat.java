
public class Stat 
{
	private int stat;
	private String name;

	
	public Stat(String name,int stat)
	{
		this.stat = stat;
		this.name = name;

	}
	
	
	public int getStat() 
	{
		return stat;
	}


	public void setStat(int stat) 
	{
		this.stat = stat;
	}
	
	


	public String getName() 
	{
		return name;
	}


	public void setName(String name) 
	{
		this.name = name;
	}


	public void calcHP(Base_Stats base, int ev, int iv, int level)
	{
		int baseHp = base.getHP();
		int hp;
		hp = (((iv+(2*baseHp)+(ev/4)+100)*level)/100 + 10);
		this.setStat(hp);
		
	}
	public void calcStat(Base_Stats base, int ev, int iv, int level, Nature x)
	{
		int baseStat = 0;
		int stat = 0;
		
		if(this.name.equals("atk"))
		{
			baseStat = base.getATK();
		}
		else if(this.name.equals("def"))
		{
			baseStat = base.getDEF();
		}
		else if(this.name.equals("spAtk"))
		{
			baseStat = base.getSPATK();
		}
		else if(this.name.equals("spDef"))
		{
			baseStat = base.getSPDEF();
		}
		else if(this.name.equals("spe"))
		{
			baseStat = base.getSPE();
		}
		
		stat = ((((iv+(2*baseStat)+ (ev/4)) * level)/100)+5);
		
		if(x.getPosStat().equals(this.name))
		{
			double increase = stat*1.1;
			stat = (int) increase;
		}
		else if(x.getNegStat().equals(this.name))
		{
			double decrease = stat*0.9;
			stat = (int)decrease;
		}
		
		this.setStat(stat);
		

		
	}
	
	
}
