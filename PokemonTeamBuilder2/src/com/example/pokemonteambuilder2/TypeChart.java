package com.example.pokemonteambuilder2;
import java.util.ArrayList;


public class TypeChart
{
	//Arraylist for storing all types
	ArrayList<Type> typeList = new ArrayList<Type>(18);

	//Arraylists for storing each type's weaknesses/resistances/immunities
	ArrayList<Type> fireWeaknesses = new ArrayList<Type>(10);
	ArrayList<Type> fireResistances = new ArrayList<Type>(10);


	ArrayList<Type> waterWeaknesses = new ArrayList<Type>(10);
	ArrayList<Type> waterResistances = new ArrayList<Type>(10);


	ArrayList<Type> grassWeaknesses = new ArrayList<Type>(10);
	ArrayList<Type> grassResistances = new ArrayList<Type>(10);


	ArrayList<Type> poisonWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> poisonResistances = new ArrayList<Type>(5);
	ArrayList<Type> poisonImmunities = new ArrayList<Type>(5);

	ArrayList<Type> fightingWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> fightingResistances = new ArrayList<Type>(5);
	ArrayList<Type> fightingImmunities = new ArrayList<Type>(5);


	ArrayList<Type> darkWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> darkResistances = new ArrayList<Type>(5);


	ArrayList<Type> normalImmunities = new ArrayList<Type>(5);
	ArrayList<Type> normalResistances = new ArrayList<Type>(5);

	ArrayList<Type> ghostImmunities = new ArrayList<Type>(5);
	ArrayList<Type> ghostWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> ghostResistances = new ArrayList<Type>(5);

	ArrayList<Type> psychicImmunities = new ArrayList<Type>(5);
	ArrayList<Type> psychicWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> psychicResistances = new ArrayList<Type>(5);

	ArrayList<Type> fairyWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> fairyResistances = new ArrayList<Type>(5);

	ArrayList<Type> dragonWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> dragonResistances = new ArrayList<Type>(5);
	ArrayList<Type> dragonImmunities = new ArrayList<Type>(5);

	ArrayList<Type> iceWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> iceResistances = new ArrayList<Type>(5);

	ArrayList<Type> electricWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> electricResistances = new ArrayList<Type>(5);
	ArrayList<Type> electricImmunities = new ArrayList<Type>(5);

	ArrayList<Type> steelWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> steelResistances = new ArrayList<Type>(5);

	ArrayList<Type> bugWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> bugResistances = new ArrayList<Type>(10);

	ArrayList<Type> rockWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> rockResistances = new ArrayList<Type>(5);

	ArrayList<Type> flyingWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> flyingResistances = new ArrayList<Type>(5);

	ArrayList<Type> groundWeaknesses = new ArrayList<Type>(5);
	ArrayList<Type> groundResistances = new ArrayList<Type>(5);
	ArrayList<Type> groundImmunities = new ArrayList<Type>(5);


	//Each different pokemon type object
	Type fire = new Type("Fire", fireWeaknesses, fireResistances, null);
	Type water = new Type("Water",waterWeaknesses,waterResistances,null);
	Type grass = new Type("Grass",grassWeaknesses,grassResistances, null);
	Type poison = new Type("Poison",poisonWeaknesses,poisonResistances, poisonImmunities);
	Type fighting = new Type("Fighting",fightingWeaknesses,fightingResistances, fightingImmunities);
	Type normal = new Type("Normal",null,normalResistances,normalImmunities);
	Type flying = new Type("Flying",flyingWeaknesses,flyingResistances,null);
	Type ground = new Type("Ground",groundWeaknesses,groundResistances,groundImmunities);
	Type rock = new Type("Rock",rockWeaknesses,rockResistances,null);
	Type bug = new Type("Bug",bugWeaknesses,bugResistances,null);
	Type ghost = new Type("Ghost",ghostWeaknesses,ghostResistances,ghostImmunities);
	Type steel = new Type("Steel",steelWeaknesses,steelResistances,null);
	Type electric = new Type("Electric",electricWeaknesses,electricResistances,electricImmunities);
	Type ice = new Type("Ice",iceWeaknesses,iceResistances,null);
	Type psychic = new Type("Psychic",psychicWeaknesses,psychicResistances,psychicImmunities);
	Type dark = new Type("Dark",darkWeaknesses,darkResistances,null);
	Type dragon = new Type("Dragon",dragonWeaknesses,dragonResistances,dragonImmunities);
	Type fairy = new Type("Fairy",fairyWeaknesses,fairyResistances,null);


	public TypeChart()
	{
		//Constructor fills all the weakness/resistance/immunity arraylists with
		//	their respective types
		normalResistances.add(rock);
		normalResistances.add(steel);
		normalImmunities.add(ghost);
		
		flyingResistances.add(rock);
		flyingResistances.add(steel);
		flyingResistances.add(electric);
		flyingWeaknesses.add(fighting);
		flyingWeaknesses.add(bug);
		flyingWeaknesses.add(grass);
		
		groundResistances.add(bug);
		groundResistances.add(grass);
		groundImmunities.add(flying);
		groundWeaknesses.add(poison);
		groundWeaknesses.add(rock);
		groundWeaknesses.add(steel);
		groundWeaknesses.add(fire);
		groundWeaknesses.add(electric);
		
		rockResistances.add(fighting);
		rockResistances.add(ground);
		rockResistances.add(steel);
		rockWeaknesses.add(flying);
		rockWeaknesses.add(bug);
		rockWeaknesses.add(fire);
		rockWeaknesses.add(ice);
		
		bugResistances.add(fighting);
		bugResistances.add(flying);
		bugResistances.add(poison);
		bugResistances.add(ghost);
		bugResistances.add(steel);
		bugResistances.add(fire);
		bugResistances.add(fairy);
		bugWeaknesses.add(grass);
		bugWeaknesses.add(psychic);
		bugWeaknesses.add(dark);
		
		ghostImmunities.add(normal);
		ghostResistances.add(dark);
		ghostWeaknesses.add(ghost);
		ghostWeaknesses.add(psychic);
		
		steelResistances.add(steel);
		steelResistances.add(fire);
		steelResistances.add(water);
		steelResistances.add(electric);
		steelWeaknesses.add(rock);
		steelWeaknesses.add(ice);
		steelWeaknesses.add(fairy);
		
		fireWeaknesses.add(grass);
		fireWeaknesses.add(bug);
		fireWeaknesses.add(steel);
		fireWeaknesses.add(ice);
		fireResistances.add(water);
		fireResistances.add(fire);
		fireResistances.add(dragon);
		fireResistances.add(rock);

		waterWeaknesses.add(fire);
		waterWeaknesses.add(ground);
		waterWeaknesses.add(rock);
		waterResistances.add(grass);
		waterResistances.add(water);
		waterResistances.add(dragon);

		grassWeaknesses.add(water);
		grassWeaknesses.add(ground);
		grassWeaknesses.add(rock);
		grassResistances.add(fire);
		grassResistances.add(poison);
		grassResistances.add(grass);
		grassResistances.add(flying);
		grassResistances.add(bug);
		grassResistances.add(steel);
		grassResistances.add(dragon);

		poisonWeaknesses.add(grass);
		poisonWeaknesses.add(fairy);
		poisonResistances.add(poison);
		poisonResistances.add(ground);
		poisonResistances.add(rock);
		poisonResistances.add(ghost);
		poisonImmunities.add(steel);

		fightingResistances.add(poison);
		fightingResistances.add(flying);
		fightingResistances.add(bug);
		fightingResistances.add(psychic);
		fightingResistances.add(fairy);
		fightingWeaknesses.add(rock);
		fightingWeaknesses.add(steel);
		fightingWeaknesses.add(normal);
		fightingWeaknesses.add(ice);
		fightingWeaknesses.add(dark);
		fightingImmunities.add(ghost);
		
		electricImmunities.add(ground);
		electricWeaknesses.add(flying);
		electricWeaknesses.add(water);
		electricResistances.add(grass);
		electricResistances.add(electric);
		electricResistances.add(dragon);
		
		psychicImmunities.add(dark);
		psychicResistances.add(steel);
		psychicResistances.add(psychic);
		psychicWeaknesses.add(fighting);
		psychicWeaknesses.add(poison);
		
		iceWeaknesses.add(flying);
		iceWeaknesses.add(grass);
		iceWeaknesses.add(ground);
		iceWeaknesses.add(dragon);
		iceResistances.add(steel);
		iceResistances.add(fire);
		iceResistances.add(water);
		iceResistances.add(ice);
		
		dragonImmunities.add(fairy);
		dragonWeaknesses.add(dragon);
		dragonResistances.add(steel);
		
		darkWeaknesses.add(ghost);
		darkWeaknesses.add(psychic);
		darkResistances.add(fighting);
		darkResistances.add(dark);
		darkResistances.add(fairy);
		
		fairyWeaknesses.add(fighting);
		fairyWeaknesses.add(dragon);
		fairyWeaknesses.add(dark);
		fairyResistances.add(poison);
		fairyResistances.add(steel);
		fairyResistances.add(fire);
		
		//Fills the typeList array with all types.
		typeList.add(bug);
		typeList.add(dark);
		typeList.add(steel);
		typeList.add(ice);
		typeList.add(fighting);
		typeList.add(flying);
		typeList.add(fire);
		typeList.add(water);
		typeList.add(grass);
		typeList.add(electric);
		typeList.add(ground);
		typeList.add(rock);
		typeList.add(fairy);
		typeList.add(dragon);
		typeList.add(ghost);
		typeList.add(normal);
		typeList.add(psychic);
		typeList.add(poison);
	}


	public ArrayList<Type> getTypeList()
	{
		return typeList;
	}


	public void setTypeList(ArrayList<Type> typeList)
	{
		this.typeList = typeList;
	}


	public Type getFire() 
	{
		return fire;
	}


	public void setFire(Type fire) 
	{
		this.fire = fire;
	}


	public Type getWater() 
	{
		return water;
	}


	public void setWater(Type water) 
	{
		this.water = water;
	}


	public Type getGrass() 
	{
		return grass;
	}


	public void setGrass(Type grass) 
	{
		this.grass = grass;
	}


	public Type getPoison() 
	{
		return poison;
	}


	public void setPoison(Type poison) 
	{
		this.poison = poison;
	}


	public Type getFighting() 
	{
		return fighting;
	}


	public void setFighting(Type fighting) 
	{
		this.fighting = fighting;
	}


	public Type getNormal() 
	{
		return normal;
	}


	public void setNormal(Type normal) 
	{
		this.normal = normal;
	}


	public Type getFlying() 
	{
		return flying;
	}


	public void setFlying(Type flying) 
	{
		this.flying = flying;
	}


	public Type getGround() 
	{
		return ground;
	}


	public void setGround(Type ground) 
	{
		this.ground = ground;
	}


	public Type getRock() 
	{
		return rock;
	}


	public void setRock(Type rock) 
	{
		this.rock = rock;
	}


	public Type getBug() 
	{
		return bug;
	}


	public void setBug(Type bug) 
	{
		this.bug = bug;
	}


	public Type getGhost() 
	{
		return ghost;
	}


	public void setGhost(Type ghost) 
	{
		this.ghost = ghost;
	}


	public Type getSteel() 
	{
		return steel;
	}


	public void setSteel(Type steel) 
	{
		this.steel = steel;
	}


	public Type getElectric() 
	{
		return electric;
	}


	public void setElectric(Type electric) 
	{
		this.electric = electric;
	}


	public Type getIce() 
	{
		return ice;
	}


	public void setIce(Type ice) 
	{
		this.ice = ice;
	}


	public Type getPsychic() 
	{
		return psychic;
	}


	public void setPsychic(Type psychic)
	{
		this.psychic = psychic;
	}


	public Type getDark() 
	{
		return dark;
	}


	public void setDark(Type dark) 
	{
		this.dark = dark;
	}


	public Type getDragon() 
	{
		return dragon;
	}


	public void setDragon(Type dragon) 
	{
		this.dragon = dragon;
	}


	public Type getFairy() 
	
	{
		return fairy;
	}


	public void setFairy(Type fairy) 
	{
		this.fairy = fairy;
	}
	
	
	
	

}
