package com.example.pokemonteambuilder2;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHandler extends SQLiteOpenHelper
{
	// All Static variables
	//Instance of the type chart for reference
	private final TypeChart typeChart = new TypeChart();
	private final ArrayList<Type> typeList = typeChart.getTypeList();

	// Database Version
	private static final int DATABASE_VERSION = 1;

	//Database Name
	private static final String DATABASE_NAME = "pokemonManager";

	//Move Table name
	private static final String TABLE_MOVES = "moves";

	//Move Table Column names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_TYPE = "type";
	private static final String KEY_CATEGORY = "category";
	private static final String KEY_PP = "pp";
	private static final String KEY_POWER = "power";
	private static final String KEY_ACCURACY = "accuracy";

	//Ability Table name
	private static final String TABLE_ABILITIES = "abilities";

	//Ability column names
	private static final String KEY_DESCRIPTION = "description";

	//Item Table name
	private static final String TABLE_ITEMS = "items";

	//Pokemon Table name
	private static final String TABLE_POKEMON = "pokemon";

	//Pokemon Table Column names
	private static final String KEY_TYPE2 = "type2";
	private static final String KEY_DEX_NUM = "dex_number";
	private static final String KEY_BASE_HP = "base_hp";
	private static final String KEY_BASE_ATK = "base_atk";
	private static final String KEY_BASE_DEF = "base_def";
	private static final String KEY_BASE_SPATK = "base_spAtk";
	private static final String KEY_BASE_SPDEF = "base_spDEf";
	private static final String KEY_BASE_SPE = "base_spe";

	//Nature Table name
	private static final String TABLE_NATURES = "natures";

	//Nature Table Column names
	private static final String KEY_POS = "positive";
	private static final String KEY_NEG = "negative";

	//Table Creating strings
	private static final String CREATE_MOVES_TABLE = "CREATE TABLE " + TABLE_MOVES + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"+ KEY_TYPE + " TEXT,"+ KEY_CATEGORY + 
			" TEXT,"+ KEY_PP + " TEXT,"+ KEY_POWER + " TEXT,"+ KEY_ACCURACY + " TEXT" + ")";

	private static final String CREATE_ABILITY_TABLE = "CREATE TABLE " + TABLE_ABILITIES + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
			+ KEY_DESCRIPTION + " TEXT" +")";

	private static final String CREATE_ITEM_TABLE = "CREATE TABLE " + TABLE_ITEMS + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
			+ KEY_DESCRIPTION + " TEXT" +")";

	private static final String CREATE_NATURE_TABLE = "CREATE TABLE " + TABLE_NATURES + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
			+ KEY_POS + " TEXT," + KEY_NEG + " TEXT" + ")";

	private static final String CREATE_POKEMON_TABLE = "CREATE TABLE " + TABLE_POKEMON + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DEX_NUM + " TEXT," + KEY_NAME + " TEXT,"
			+ KEY_TYPE + " TEXT," + KEY_TYPE2 + " TEXT," + KEY_BASE_HP + " TEXT," + KEY_BASE_ATK + " TEXT," + KEY_BASE_DEF + " TEXT," + KEY_BASE_SPATK + " TEXT," +
			KEY_BASE_SPDEF + " TEXT," + KEY_BASE_SPE + " TEXT" +")";



	public DatabaseHandler(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	//Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(CREATE_ABILITY_TABLE);
		db.execSQL(CREATE_MOVES_TABLE);
		db.execSQL(CREATE_ITEM_TABLE);
		db.execSQL(CREATE_NATURE_TABLE);
		db.execSQL(CREATE_POKEMON_TABLE);
	}

	//Upgrading Database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersiom)
	{
		//Drop older tables if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOVES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ABILITIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NATURES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POKEMON);

		//Create tables again
		onCreate(db);

	}

	//Adding a new Move
	public void addMove(Move move)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		//Move attributes
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, move.getName());
		values.put(KEY_TYPE, move.getType().getName());
		values.put(KEY_CATEGORY, move.getCategory());
		values.put(KEY_PP, move.getPp());
		values.put(KEY_POWER, move.getPower());
		values.put(KEY_ACCURACY, move.getAccuracy());

		//Inserting row
		db.insert(TABLE_MOVES, null, values);
		db.close(); //Closing database connection
	}

	//Adding a new Item
	public void addItem(Item item)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		//Item attributes
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, item.getName());
		values.put(KEY_DESCRIPTION, item.getDescription());

		//Inserting row
		db.insert(TABLE_ITEMS, null, values);
		db.close(); //Closing database connection
	}

	//Adding a new Ability
	public void addAbility(Ability ability)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		//Ability attributes
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, ability.getName());
		values.put(KEY_DESCRIPTION, ability.getDescription());

		//Inserting row
		db.insert(TABLE_ABILITIES, null, values);
		db.close(); //Closing database connection
	}

	//Adding a new Nature
	public void addNature(Nature nature)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		//Nature attributes
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, nature.getName());
		values.put(KEY_POS, nature.getPosStat());
		values.put(KEY_NEG, nature.getNegStat());

		//Inserting row
		db.insert(TABLE_NATURES, null, values);
		db.close(); //Closing database connection
	}

	//Adding a new Pokemon
	public void addPokemon(Pokemon pokemon)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		//Pokemon attributes
		ContentValues values = new ContentValues();
		values.put(KEY_DEX_NUM, pokemon.getDEX_NUM());
		values.put(KEY_NAME, pokemon.getName());
		values.put(KEY_TYPE, pokemon.getT1().getName());
		if(pokemon.getT2()!=null)
		{
			values.put(KEY_TYPE2, pokemon.getT2().getName());
		}
		values.put(KEY_BASE_HP, pokemon.getBase().getHP());
		values.put(KEY_BASE_ATK, pokemon.getBase().getATK());
		values.put(KEY_BASE_DEF, pokemon.getBase().getDEF());
		values.put(KEY_BASE_SPATK, pokemon.getBase().getSPATK());
		values.put(KEY_BASE_SPDEF, pokemon.getBase().getSPDEF());
		values.put(KEY_BASE_SPE, pokemon.getBase().getSPE());

		//Inserting row
		db.insert(TABLE_POKEMON, null, values);
		db.close(); //Closing database connection
	}

	//getting a single move
	public Move getMove(int id)
	{
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT * FROM " + TABLE_MOVES + " WHERE " + KEY_ID + " = " + id;

		Cursor c = db.rawQuery(selectQuery, null);

		if(c!= null)
		{
			c.moveToFirst();
		}

		Move move = new Move();
		move.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		move.setName(c.getString(c.getColumnIndex(KEY_NAME)));
		move.setCategory(c.getString(c.getColumnIndex(KEY_CATEGORY)));
		move.setAccuracy(c.getInt(c.getColumnIndex(KEY_ACCURACY)));
		move.setPower(c.getInt(c.getColumnIndex(KEY_POWER)));
		move.setPp(c.getInt(c.getColumnIndex(KEY_PP)));
		for(Type x : typeList)
		{
			if(x.getName().equals(c.getString(c.getColumnIndex(KEY_TYPE))))
			{
				move.setType(x);
			}
		}
		return move;
	}

	//Getting all moves
	public List<Move> getAllMoves()
	{
		List<Move> moves = new ArrayList<Move>();
		String selectQuery = "SELECT * FROM " + TABLE_MOVES;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		//Looping through all rows and adding to list
		if(c.moveToFirst())
		{
			do
			{
				Move move = new Move();
				move.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				move.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				move.setCategory(c.getString(c.getColumnIndex(KEY_CATEGORY)));
				move.setAccuracy(c.getInt(c.getColumnIndex(KEY_ACCURACY)));
				move.setPower(c.getInt(c.getColumnIndex(KEY_POWER)));
				move.setPp(c.getInt(c.getColumnIndex(KEY_PP)));
				for(Type x : typeList)
				{
					if(x.getName().equals(c.getString(c.getColumnIndex(KEY_TYPE))))
					{
						move.setType(x);
					}
				}

				//Adding to move list
				moves.add(move);
			}
			while(c.moveToNext());
		}
		return moves;
	}

	//Updating a single Move
	public int updateMove(Move move)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, move.getName());
		values.put(KEY_CATEGORY, move.getCategory());
		values.put(KEY_ACCURACY, move.getAccuracy());
		values.put(KEY_POWER, move.getPower());
		values.put(KEY_PP, move.getPp());
		values.put(KEY_TYPE, move.getType().getName());

		//Updating row
		return db.update(TABLE_MOVES, values, KEY_ID + " =?", new String[] {String.valueOf(move.getId())});
	}

	//Deleting a single Move
	public void deleteMove(int move_id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_MOVES, KEY_ID + " =?", new String[] {String.valueOf(move_id)});
		db.close();
	}

	//Getting a single item
	public Item getItem(int itemId)
	{
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT * FROM " + TABLE_ITEMS + " WHERE " + KEY_ID + " = " + itemId;

		Cursor c = db.rawQuery(selectQuery, null);

		if(c != null)
		{
			c.moveToFirst();
		}

		Item item = new Item();
		item.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		item.setName(c.getString(c.getColumnIndex(KEY_NAME)));
		item.setDescription(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));

		return item;
	}

	//Getting all items
	public List<Item> getAllItems()
	{
		List<Item> items = new ArrayList<Item>();
		String selectQuery = "SELECT * FROM " + TABLE_ITEMS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);


		//Looping through all rows and adding to list
		if(c.moveToFirst())
		{
			do
			{
				Item item = new Item();
				item.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				item.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				item.setDescription(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));

				//adding to item list
				items.add(item);
			}
			while(c.moveToNext());
		}

		return items;
	}

	//Updating an item
	public int updateItem(Item item)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, item.getName());
		values.put(KEY_DESCRIPTION, item.getDescription());

		//updating row
		return db.update(TABLE_ITEMS, values, KEY_ID + " =?", new String[] {String.valueOf(item.getId())});
	}

	//Deleting an item
	public void deleteItem(int itemId)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_ITEMS, KEY_ID + " =?", new String[] {String.valueOf(itemId)});
		db.close();
	}

	//Getting a single ability
	public Ability getAbility(int id)
	{
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT * FROM " + TABLE_ABILITIES + " WHERE " + KEY_ID + " = " + id;

		Cursor c = db.rawQuery(selectQuery, null);

		if(c!=null)
		{
			c.moveToFirst();
		}

		Ability ability = new Ability();
		ability.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		ability.setName(c.getString(c.getColumnIndex(KEY_NAME)));
		ability.setDescription(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));

		return ability;

	}

	//Getting all abilities
	public List<Ability> getAllAbilities()
	{
		List<Ability> abilities = new ArrayList<Ability>();

		String selectQuery = "SELECT * FROM " + TABLE_ABILITIES;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		//Looping through all rows and adding to list
		if(c.moveToFirst())
		{
			do
			{
				Ability ability = new Ability();
				ability.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				ability.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				ability.setDescription(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));

				abilities.add(ability);
			}
			while(c.moveToNext());
		}

		return abilities;
	}

	//Updating a single ability
	public int updateAbility(Ability ability)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, ability.getName());
		values.put(KEY_DESCRIPTION, ability.getDescription());

		return db.update(TABLE_ABILITIES, values, KEY_ID + " =?", new String[] {String.valueOf(ability.getId())});
	}

	//delete a single ability
	public void deleteAbility(int id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_ABILITIES, KEY_ID + " =?", new String[] {String.valueOf(id)});
		db.close();
	}


	//Getting a single nature
	public Nature getNature(int id)
	{
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NATURES + " WHERE " + KEY_ID + " = " + id;

		Cursor c = db.rawQuery(selectQuery, null);

		if(c!=null)
		{
			c.moveToFirst();
		}

		Nature nature = new Nature();
		nature.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		nature.setName(c.getString(c.getColumnIndex(KEY_NAME)));
		nature.setNegStat(c.getString(c.getColumnIndex(KEY_NEG)));
		nature.setPosStat(c.getString(c.getColumnIndex(KEY_POS)));

		return nature;

	}

	//Getting all natures
	public List<Nature> getAllnatures()
	{
		List<Nature> natures = new ArrayList<Nature>();

		String selectQuery = "SELECT * FROM " + TABLE_NATURES;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		//Loooping through all rows and adding to list
		if(c.moveToFirst())
		{
			do
			{
				Nature nature = new Nature();
				nature.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				nature.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				nature.setNegStat(c.getString(c.getColumnIndex(KEY_NEG)));
				nature.setPosStat(c.getString(c.getColumnIndex(KEY_POS)));

				//Adding to nature list
				natures.add(nature);
			}
			while(c.moveToNext());
		}

		//return nature list
		return natures;

	}

	//Updating a single nature
	public int updateNature(Nature nature)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME,nature.getName());
		values.put(KEY_POS, nature.getPosStat());
		values.put(KEY_NEG, nature.getNegStat());

		//Updating row
		return db.update(TABLE_NATURES, values, KEY_ID + " =?", new String[] {String.valueOf(nature.getId())});

	}

	//Deleting a single Nature
	public void deleteNature(Nature nature)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_NATURES, KEY_ID + " = ?", new String[]{String.valueOf(nature.getId())});
		db.close();
	}

	//Getting a single Pokemon
	public Pokemon getPokemon(int id)
	{
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_POKEMON + " WHERE " + KEY_ID + " = " + id;

		Cursor c = db.rawQuery(selectQuery, null);

		if(c!=null)
		{
			c.moveToFirst();
		}

		Pokemon pn = new Pokemon();
		pn.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		pn.setDEX_NUM(c.getInt(c.getColumnIndex(KEY_DEX_NUM)));
		pn.setName(c.getString(c.getColumnIndex(KEY_NAME)));

		//Creating variables to set values to
		int hp = c.getInt(c.getColumnIndex(KEY_BASE_HP));
		int atk = c.getInt(c.getColumnIndex(KEY_BASE_ATK));
		int def = c.getInt(c.getColumnIndex(KEY_BASE_DEF));
		int spAtk = c.getInt(c.getColumnIndex(KEY_BASE_SPATK));
		int spDef = c.getInt(c.getColumnIndex(KEY_BASE_SPDEF));
		int spe = c.getInt(c.getColumnIndex(KEY_BASE_SPE));

		pn.getBase().setATK(atk);
		pn.getBase().setDEF(def);
		pn.getBase().setHP(hp);
		pn.getBase().setSPATK(spAtk);
		pn.getBase().setSPDEF(spDef);
		pn.getBase().setSPE(spe);

		//Looping through typeList to find the pokemon's type and assign it to the pokemon
		for(Type x : typeList)
		{
			if(x.getName().equals(c.getString(c.getColumnIndex(KEY_TYPE))))
			{
				pn.setT1(x);
			}
			if(x.getName().equals(c.getString(c.getColumnIndex(KEY_TYPE2))))
			{
				pn.setT2(x);
			}
			else
			{
				pn.setT2(pn.getT1());
			}
		}

		return pn;

	}

	//Getting all Pokemon
	public List<Pokemon> getAllPokemon()
	{
		List<Pokemon> pokemonList = new ArrayList<Pokemon>();

		String selectQuery = "SELECT * FROM " + TABLE_POKEMON;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		//Looping through all rows and adding to list
		if(c.moveToFirst())
		{
			do
			{
				Pokemon pn = new Pokemon();
				pn.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				pn.setDEX_NUM(c.getInt(c.getColumnIndex(KEY_DEX_NUM)));
				pn.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				pn.getBase().setATK(c.getInt(c.getColumnIndex(KEY_BASE_ATK)));
				pn.getBase().setDEF(c.getInt(c.getColumnIndex(KEY_BASE_DEF)));
				pn.getBase().setHP(c.getInt(c.getColumnIndex(KEY_BASE_HP)));
				pn.getBase().setSPATK(c.getInt(c.getColumnIndex(KEY_BASE_SPATK)));
				pn.getBase().setSPDEF(c.getInt(c.getColumnIndex(KEY_BASE_SPDEF)));
				pn.getBase().setSPE(c.getInt(c.getColumnIndex(KEY_BASE_SPE)));

				//Looping through typeList to find the pokemon's type and assign it to the pokemon
				for(Type x : typeList)
				{
					if(x.getName().equals(c.getString(c.getColumnIndex(KEY_TYPE))))
					{
						pn.setT1(x);
					}
					if(x.getName().equals(c.getString(c.getColumnIndex(KEY_TYPE2))))
					{
						pn.setT2(x);
					}
					else
					{
						pn.setT2(pn.getT1());
					}
				} 

				pokemonList.add(pn);
			}
			while(c.moveToNext());
		}

		return pokemonList;
	}

	//Update a single Pokemon
	public int updatePokemon(Pokemon pn)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(KEY_DEX_NUM, pn.getDEX_NUM());
		values.put(KEY_NAME, pn.getName());
		values.put(KEY_TYPE, pn.getT1().getName());
		values.put(KEY_TYPE2, pn.getT2().getName());
		values.put(KEY_BASE_HP, pn.getBase().getHP());
		values.put(KEY_BASE_ATK, pn.getBase().getATK());
		values.put(KEY_BASE_DEF, pn.getBase().getDEF());
		values.put(KEY_BASE_SPATK, pn.getBase().getSPATK());
		values.put(KEY_BASE_SPDEF, pn.getBase().getSPDEF());
		values.put(KEY_BASE_SPE, pn.getBase().getSPE());

		//Updating row
		return db.update(TABLE_POKEMON, values, KEY_ID + " = ?", new String[] {String.valueOf(pn.getId())});
	}

	//Delete a single Pokemon
	public void deletePokemon(Pokemon pn)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_POKEMON, KEY_ID + " = ?", new String[] {String.valueOf(pn.getId())});
		db.close();
	}

	public TypeChart getTypeChart() {
		return typeChart;
	}

	public ArrayList<Type> getTypeList() {
		return typeList;
	}







}
