package com.example.pokemonteambuilder2;

public class Base_Stats
{
	private int HP;
	private int ATK;
	private int DEF;
	private int SPATK;
	private int SPDEF;
	private int SPE;
	
	public Base_Stats(int HP, int ATK, int DEF,int SPATK, int SPDEF, int SPE)
	{
		this.HP = HP;
		this.ATK = ATK;
		this.DEF = DEF;
		this.SPATK = SPATK;
		this.SPDEF = SPDEF;
		this.SPE = SPE;
	}
	
	public Base_Stats()
	{
		
	}
	
	public int getHP() 
	{
		return HP;
	}
	public int getATK() 
	{
		return ATK;
	}
	public int getDEF() 
	{
		return DEF;
	}
	public int getSPATK() 
	{
		return SPATK;
	}
	public int getSPDEF() 
	{
		return SPDEF;
	}
	public int getSPE() 
	{
		return SPE;
	}

	public void setHP(int hP)
	{
		HP = hP;
	}

	public void setATK(int aTK)
	{
		ATK = aTK;
	}

	public void setDEF(int dEF)
	{
		DEF = dEF;
	}

	public void setSPATK(int sPATK)
	{
		SPATK = sPATK;
	}

	public void setSPDEF(int sPDEF)
	{
		SPDEF = sPDEF;
	}

	public void setSPE(int sPE)
	{
		SPE = sPE;
	}
	
	public void setBase(int hp, int atk, int def, int spAtk, int spDef, int spe)
	{
		HP = hp;
		ATK = atk;
		DEF = def;
		SPATK = spAtk;
		SPDEF = spDef;
		SPE = spe;
	}
	
	
}
