package com.example.pokemonteambuilder2;

public class Move
{
	private int id;
	private String name;
	private Type type;
	private String category;
	private int pp;
	private int power;
	private int accuracy;
	
	public Move(int id, String name, Type type, String category, int pp, int power, int accuracy)
	{
		this.id = id;
		this.name = name;
		this.type = type;
		this.category = category;
		this.pp = pp;
		this.power = power;
		this.accuracy = accuracy;
	}
	
	public Move(String name, Type type, String category, int pp, int power, int accuracy)
	{
		this.name = name;
		this.type = type;
		this.category = category;
		this.pp = pp;
		this.power = power;
		this.accuracy = accuracy;
	}
	
	public Move()
	{
		
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Type getType()
	{
		return type;
	}

	public void setType(Type type)
	{
		this.type = type;
	}

	public String getCategory()
	{
		return category;
	}

	public void setCategory(String category)
	{
		this.category = category;
	}

	public int getPp()
	{
		return pp;
	}

	public void setPp(int pp)
	{
		this.pp = pp;
	}

	public int getPower()
	{
		return power;
	}

	public void setPower(int power)
	{
		this.power = power;
	}

	public int getAccuracy()
	{
		return accuracy;
	}

	public void setAccuracy(int accuracy)
	{
		this.accuracy = accuracy;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	
	
	
}
