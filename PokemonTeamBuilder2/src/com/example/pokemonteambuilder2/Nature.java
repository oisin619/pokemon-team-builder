package com.example.pokemonteambuilder2;

public class Nature 
{
	private int id;
	private String name;
	private String posStat;
	private String negStat;
	
	public Nature(int id, String name, String posStat, String negStat)
	{
		this.id = id;
		this.name = name;
		this.posStat = posStat;
		this.negStat = negStat;
	}
	
	public Nature(String name, String posStat, String negStat)
	{
		this.name = name;
		this.posStat = posStat;
		this.negStat = negStat;
	}
	
	public Nature()
	{
		
	}
	
	
	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}


	public String getPosStat() 
	{
		return posStat;
	}

	public void setPosStat(String posStat) 
	{
		this.posStat = posStat;
	}

	public String getNegStat() 
	{
		return negStat;
	}

	public void setNegStat(String negStat) 
	{
		this.negStat = negStat;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	
	
}
