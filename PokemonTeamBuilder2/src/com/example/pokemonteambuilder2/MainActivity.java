package com.example.pokemonteambuilder2;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity 
{


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		DatabaseHandler db = new DatabaseHandler(this);
		TextView test = (TextView)findViewById(R.id.printDetails);
		
		//Inserting sample data
		db.addItem(new Item("Sitris Berry", "A Berry to be consumed by Pok�mon. If a Pok�mon holds one, it can restore its own HP by 25% during battle."));
		db.addNature(new Nature("Jolly","spe","spAtk"));
		db.addNature(new Nature("Modest","spAtk", "atk"));
		db.addAbility(new Ability("Blaze","Fire-type moves will increase by 1.5x if the user has less than or equal to 1/3 of its maximum HP remaining."));
		db.addAbility(new Ability("Overgrow", "Grass-type moves will increase by 1.5x if the user has less than or equal to 1/3 of its maximum HP remaining."));
		db.addPokemon(new Pokemon(1,"Bulbasaur",db.getTypeChart().getGrass(),db.getTypeChart().getPoison(),new Base_Stats(45,49,49,65,65,45)));
		db.addPokemon(new Pokemon(4,"Charmander",db.getTypeChart().getFire(),null,new Base_Stats(39,52,43,60,50,65)));
		db.addMove(new Move("Energy Ball",db.getTypeChart().getGrass(),"Special",10,90,100));
		db.addMove(new Move("Flamethrower",db.getTypeChart().getFire(),"Special",15,90,100));
		
		Pokemon testMon = db.getPokemon(1);
		Item meh = db.getItem(1);
		testMon.setAbility(db.getAbility(2));
		testMon.setNature(db.getNature(2));
		testMon.setMove(db.getMove(1));
		
		String typeText ="";
		if(testMon.getT2()!=testMon.getT1())
		{
			typeText = testMon.getT1().getName()+"/"+testMon.getT2().getName();
		}
		else
		{
			typeText = testMon.getT1().getName();
		}
		
		test.setText("Pokemon: "+testMon.getName() + "\nDex Number: "+testMon.getDEX_NUM()+
				"\nType: "+typeText + "\nNature: "+testMon.getNature().getName()+"\nAbility: "+testMon.getAbility().getName()
				+"\nMove 1: "+ testMon.getMove1().getName());
		
		
		
		
	}


}
