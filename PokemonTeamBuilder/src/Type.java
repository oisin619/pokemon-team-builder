import java.lang.reflect.Array;
import java.util.ArrayList;


public class Type 
{
	private String name;
	final double weakness = 2;
	final double resistance = 0.5;
	final double normal = 1;
	final double immune = 0;
	ArrayList<Type> weaknesses = new ArrayList<Type>(10);
	ArrayList<Type> resistances = new ArrayList<Type>(10);
	ArrayList<Type> immunities = new ArrayList<Type>(10);

	public Type(String name, ArrayList<Type> weaknesses, ArrayList<Type> resistances, ArrayList<Type> immunities)
	{
		this.name = name;
		this.weaknesses = weaknesses;
		this.resistances = resistances;
		this.immunities = immunities;
	}
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	public double getWeakness() 
	{
		return weakness;
	}
	public double getResistance() 
	{
		return resistance;
	}
	public double getNormal() 
	{
		return normal;
	}
	public double getImmune() 
	{
		return immune;
	}

	public ArrayList<Type> getWeaknesses()
	{
		return weaknesses;
	}
	public void setWeaknesses(ArrayList<Type> weaknesses)
	{
		this.weaknesses = weaknesses;
	}
	public ArrayList<Type> getResistances()
	{
		return resistances;
	}
	public void setResistances(ArrayList<Type> resistances)
	{
		this.resistances = resistances;
	}
	public ArrayList<Type> getImmunities()
	{
		return immunities;
	}
	public void setImmunities(ArrayList<Type> immunities)
	{
		this.immunities = immunities;
	}
	public double calcDamage(Type def1, Type def2)
	{
		double damage = normal;
		if(def2 == null)
		{
			damage = this.checkMatchUp(def1);
		}
		else
		{
			damage = this.checkMatchUp(def1) * this.checkMatchUp(def2);
		}
		return damage;
	}
	public double checkMatchUp(Type def)
	{
		double damage = normal;
		if(weaknesses != null)
		{
			for(Type x : weaknesses)
			{
				if(def.equals(x))
				{
					damage = weakness;
					break;
				}
			}
		}
		
		if(resistances != null)
		{
			for(Type x :resistances)
			{
				if(def.equals(x))
				{
					damage = resistance;
					break;
				}
			}
		}
		
		if(immunities != null)
		{
			for(Type x : immunities)
			{
				if(def.equals(x))
				{
					damage = immune;
					break;
				}
			}
		}
		return damage;
	}


}
